Permet de vérifier si une IP à le droit de faire quelque chose si elle est
dans un domain accepter.

Pour cela il faut un fichier de configuration, on les dommaines acceptable
ou non sont listé, par exemple `/etc/postfix/relay-domains`

```
mon-joli-domain.com	OK
bad-domain.com		REJECT
```

Il faut lancer le daemon (mettre le fichier ip-from-dns.service dans
/etc/systemd/system):
```
systemctl enable ip-from-dns.service
systemctl start ip-from-dns.service
```

Vous pouvez tester votre configuration avec:
```
postmap -q 1.2.3.4 socketmap:unix:/var/spool/postfix/postfix-ipfromdns/socket:ipfromdns
```
Si l'ip est retrouvé dans un des domaines la réponse sera celle demandée
(`OK` ou `REJECT`) sinon on reçoit un `NOTFOUND`
  
ensuite il faut configurer postfix pour utiliser ce daemon:
```
smtpd_relay_restrictions = 
   ...
   check_client_access socketmap:unix:/postfix-ipfromdns/socket:ipfromdns

smtpd_recipient_restrictions =
   ...
   check_client_access socketmap:unix:/postfix-ipfromdns/socket:ipfromdns
```

recharger la configuration postfix:
```
systemctl reload postfix
```

Dès que vous modifier le fichier /etc/postfix/relay-domains, les
modifications sont prise en compte, il n'y a rien a recharger.

Il est possible de modifier certaine chose en ajoutant des variables
d'environnement dans ip-from-dns.service via par exemple:
```
systemctl edit ip-from-dns.service
```

les variables modifiables sont (elles se trouvent en haut du fichier
ip-from-dns) (avec leur valeur par defaut):
```
DNS_SERVER=

DOMAIN_FILE="etc/postfix/relay-domains"

SOCKETMAP_NAME="ipfromdns"

SOCKETMAP_SOCKET_TYPE="unix"

SOCKETMAP_UNIX_SOCKET_PATH="/var/spool/postfix/postfix-$SOCKETMAP_NAME/socket"
SOCKETMAP_UNIX_SOCKET_OWNER_UID="postfix"
SOCKETMAP_UNIX_SOCKET_OWNER_GID="postfix"
SOCKETMAP_UNIX_SOCKET_MODE="0660"

SOCKETMAP_INET_INTERFACE="127.0.0.1"
SOCKETMAP_INET_PORT="10000"
```
